package common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.ServletContext;

public class JDBCconnect {
	public Connection con;
	public Statement stmt;
	public PreparedStatement psmt;
	public ResultSet rs;
	
	public JDBCconnect(ServletContext application) {
		
	    String DBDriver = application.getInitParameter("DBDriver");
	    String DBUrl= application.getInitParameter("DBUrl");
	    String DBId = application.getInitParameter("DBId");
	    String DBPwd = application.getInitParameter("DBPwd");
	    
		try {
			
			// JDBC 드라이버 로도
			Class.forName(DBDriver);
			
			// DB에 연결
			con = DriverManager.getConnection(DBUrl, DBId, DBPwd);
			System.out.println("DB 연결");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	

	


	public void close() {
		try {
			if(rs != null) rs.close();
			if(stmt != null) stmt.close();
			if(psmt !=null) psmt.close();
			if(con != null) con.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

}
