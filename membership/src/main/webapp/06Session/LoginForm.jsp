<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<title>Session</title>
</head>
<body>
	<jsp:include page="../Common/Link.jsp" />
	<h2>로그인 페이지</h2>

	<span> <%=request.getAttribute("LoginErrMsg") == null ? "" : request.getAttribute("LoginErrMsg")%>
	
	<span><%=session.getAttribute("UserId") %></span>
	</span>

	<%
	if (session.getAttribute("UserId") == null) {
		// 로그아웃 상태
	%>

	<script type="text/javascript">
		function validateForm(form) {
			if (!form.user_id.value) {
				alert("아이디를 입력하세요.");
				return false;
			}

			if (form.user_pw.value == "") {
				alert("패스워드를 입력하세요")
				return false;
			}
		}
	</script>

	<form action="LoginProcess.jsp" method="post" name="loginFrm"
		onSubmit="return validateForm(this);">
		아이디 : <input type="text" name="user_id" /><br /> 패스워드 : <input
			type="password" name="user_pw" /> <br /> <input type="submit"
			value="로그인하기" />
	</form>

	<%
	} else {
	%>
	<%=session.getAttribute("UserName")%>
	회원님, 로그인하셨습니다.
	<br />
	<a href="Logout.jsp">[로그아웃]</a>
	<%
	}
	%>


</body>
</html>